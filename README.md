# README

### Pipeline and Coverage
[![pipeline status](https://gitlab.com/Fabiano_Alves/mlabs_backend_test/badges/master/pipeline.svg)](https://gitlab.com/Fabiano_Alves/mlabs_backend_test/commits/master) [![coverage report](https://gitlab.com/Fabiano_Alves/mlabs_backend_test/badges/master/coverage.svg)](https://gitlab.com/Fabiano_Alves/mlabs_backend_test/commits/master)

### Setup using Docker
#### In order to setup the project you need to follow these steps:

0. Make sure your environment is provided with the last docker/docker-compose software versions.

1. Run `docker-compose build` and take a :coffee:

2. Run `docker-compose run --rm app bash` and run this:

	2.1. Run `bundle`

	2.2. Run `bin/rails db:create db:migrate`

	2.3. Run `RAILS_ENV=test bin/rails db:create db:migrate`

3. Test your environment:

	3.1 Run the server using `docker-compose up` (use with -d to keep it in 	background)

	3.2 Run the tests `docker-compose run app bin/rspec` or inside a bash container.