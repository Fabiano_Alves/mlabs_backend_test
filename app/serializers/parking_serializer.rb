class ParkingSerializer < ActiveModel::Serializer
  include ActionView::Helpers::DateHelper

  attributes :id, :time, :paid, :left, :entry, :out

  def time
    return 'parked' if object.out.nil?

    distance_of_time_in_words(object.entry, object.out)
  end

  def entry
    object.entry.strftime('%d/%m/%Y %H:%M:%S')
  end

  def out
    object.out.try(:strftime, '%d/%m/%Y %H:%M:%S')
  end
end
