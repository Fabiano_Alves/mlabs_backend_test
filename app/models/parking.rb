class Parking < ApplicationRecord
  validates_each :plate do |record, _attr, value|
    error = I18n.t('errors.parking.plate_format')

    record.errors.add(:plate, error) unless Plate.valid?(value)
  end
end
