class ParkingController < ApplicationController
  before_action :set_parking, only: %i[pay out]

  def show
    @parkings = Parking.where(plate: params[:plate])

    raise_record_not_found unless @parkings.present?

    render json: @parkings, status: :ok
  end

  def create
    @parking = Parking.create(parking_params)

    render json: @parking, status: :created
  end

  def pay
    @parking.update(paid: true) unless @parking.paid?

    render json: @parking, status: :ok
  end

  def out
    if @parking.paid?
      @parking.update(left: false, out: Time.zone.now) if @parking.left?

      render json: @parking, status: :ok
    else
      message = I18n.t('errors.parking.unpaid_error')

      render json: { error: message }, status: :forbidden
    end
  end

  private

  def set_parking
    @parking = Parking.find(params[:id])

    raise_record_not_found unless @parking.present?
  end

  def raise_record_not_found
    raise ActiveRecord::RecordNotFound
  end

  def parking_params
    params.permit(:plate)
  end
end
