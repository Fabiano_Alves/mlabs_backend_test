class ApplicationController < ActionController::API
  include Exceptions
  before_action :validate_plate!, only: %i[show create]

  rescue_from ActiveRecord::RecordNotFound, with: :not_found
  rescue_from Exceptions::InvalidParameter, with: :invalid_param_error

  def validate_plate!
    return if valid_plate?

    raise Exceptions::InvalidParameter, I18n.t('errors.parking.plate_format')
  end

  def valid_plate?
    Plate.valid?(params[:plate])
  end

  def not_found
    render status: :not_found, json: ''
  end

  def invalid_param_error(exception)
    render status: :unprocessable_entity, json: { error: exception.message }
  end
end
