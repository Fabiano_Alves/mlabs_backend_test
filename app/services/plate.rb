class Plate
  def self.valid?(plate)
    new(plate).valid?
  end

  def initialize(plate)
    @plate = plate
  end

  def valid?
    return false unless plate.present?

    plate[regex] ? true : false
  end

  private

  attr_accessor :plate

  def regex
    /\A[A-Za-z]{3}-\d{4}\z/
  end
end
