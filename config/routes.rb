Rails.application.routes.draw do
  resources :parking, only: %i[create show], param: :plate

  put 'parking/:id/pay', to: 'parking#pay', as: :pay
  put 'parking/:id/out', to: 'parking#out', as: :out
end
