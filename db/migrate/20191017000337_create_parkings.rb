class CreateParkings < ActiveRecord::Migration[5.1]
  def change
    create_table :parkings do |t|
      t.string   :plate
      t.boolean  :paid,   default: false
      t.boolean  :left,   default: true
      t.datetime :entry,  default: Time.zone.now
      t.datetime :out

      t.timestamps
    end
  end
end
