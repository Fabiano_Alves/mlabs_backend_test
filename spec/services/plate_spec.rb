require 'rails_helper'

RSpec.describe Plate do
  subject { described_class.valid?(plate) }

  context 'when plate is invalid' do
    invalid_plates.each do |invalid_plate|
      let(:plate) { invalid_plate }

      it { expect(subject).to be_falsey }
    end
  end

  context 'when plate is valid' do
    valid_plates.each do |valid_plate|
      let(:plate) { valid_plate }

      it { expect(subject).to be_truthy }
    end
  end
end
