def valid_plates
  %w[HHH-0000 hhh-0000 HhH-0000]
end

def invalid_plates
  ['000-0000', 'HHH-H000', 'HHH0000', '', nil]
end
