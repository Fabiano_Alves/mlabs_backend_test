require 'rails_helper'

RSpec.shared_examples 'application_controller' do
  describe 'rescues from ActiveRecord::RecordNotFound' do
    context 'on GET #show' do
      let(:plate) { valid_plates.sample }

      before { get :show, params: { plate: plate, format: :json } }

      it { expect(response.status).to eq(404) }
      it { expect(response.body).to be_blank }
    end
  end

  describe 'rescues from ActionController::InvalidParameter' do
    context 'on POST #create' do
      let(:plate) { invalid_plates.sample }

      before { post :create, params: { plate: plate }, format: :json }

      it { expect(response.status).to eq(422) }
      it { expect(response.body).to match(/error/) }
    end
  end
end
