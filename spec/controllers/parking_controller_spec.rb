require 'rails_helper'

RSpec.describe ParkingController, type: :controller do
  it_behaves_like 'application_controller'

  let(:plate) { valid_plates.sample }
  let(:body_hash) { JSON.parse(response.body) }

  context 'on POST #create' do
    let(:parking) { Parking.last }
    let(:serialized_parking) { ParkingSerializer.new(parking).to_json }

    before { post :create, params: { plate: plate, format: :json } }

    it { expect(response.status).to eq(201) }
    it { expect(response.body).to eq(serialized_parking) }
    it { expect(body_hash['time']).to eq('parked') }
    it { expect(body_hash['paid']).to eq(false) }
    it { expect(body_hash['left']).to eq(true) }
  end

  context 'on GET #show' do
    let(:history) { Parking.where(plate: plate) }
    let(:total_time) { distance_of_time_in_words(parking.entry, parking.out) }
    let!(:parking) do
      create(:parking, plate: plate, paid: true, out: Time.zone.now)
    end
    let(:collection) do
      ActiveModelSerializers::SerializableResource.new(history).to_json
    end

    before { get :show, params: { plate: plate, format: :json } }

    it { expect(response.status).to eq(200) }
    it { expect(response.body).to eq(collection) }
    it { expect(body_hash.first['time']).to eq(total_time) }
  end

  context 'on PUT #pay' do
    let(:parking) { create(:parking, paid: false, left: false) }
    let(:serialized_parking) { ParkingSerializer.new(parking.reload).to_json }

    before { put :pay, params: { id: parking.id, format: :json } }

    it { expect(response.status).to eq(200) }
    it { expect(response.body).to eq(serialized_parking) }
    it { expect(body_hash['paid']).to be_truthy }
    it { expect(body_hash['left']).to be_falsey }
    it { expect(body_hash['out']).to be_nil }
  end

  context 'on PUT #out' do
    let(:serialized_parking) { ParkingSerializer.new(parking.reload).to_json }
    let(:parking) { create(:parking, paid: paid, left: false) }

    before { put :out, params: { id: parking.id, format: :json } }

    context 'when parking has paid' do
      let(:paid) { true }

      before { put :out, params: { id: parking.id, format: :json } }

      it { expect(response.status).to eq(200) }
      it { expect(response.body).to eq(serialized_parking) }
      it { expect(body_hash['paid']).to be_truthy }
      it { expect(body_hash['left']).to be_falsey }
    end

    context 'when parking no has paid' do
      let(:paid) { false }
      let(:error) { { error: I18n.t('errors.parking.unpaid_error') }.to_json }

      it { expect(response.status).to eq(403) }
      it { expect(response.body).to eq(error) }
    end
  end
end
