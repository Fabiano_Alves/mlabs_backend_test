require 'rails_helper'

RSpec.describe Parking, type: :model do
  let(:parking) { build(:parking) }

  context 'validate format of plate' do
    valid_plates.each do |valid_plate|
      it { should allow_value(valid_plate).for(:plate) }
    end

    invalid_plates.each do |invalid_plate|
      it { should_not allow_value(invalid_plate).for(:plate) }
    end
  end

  context 'when has a valid plate' do
    it { expect(parking).to be_valid }
  end

  context 'when has a invalid plate' do
    let(:parking) { build(:parking, plate: invalid_plates.sample) }

    it { expect(parking).not_to be_valid }
  end

  it 'has correct fields' do
    expect(parking).to respond_to(:plate, :paid, :left, :entry, :out)
  end
end
