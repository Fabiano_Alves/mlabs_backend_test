FactoryBot.define do
  factory :parking do
    plate { valid_plates.sample }
    paid { false }
    left { true }
    entry { 25.minutes.ago }
    out { nil }
  end
end
